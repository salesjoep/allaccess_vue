# allaccess_vue2
Allaccess_vue2 is a Vue project that is connected with a Spring Boot REST API.

## Usage
Download or clone this repository and download or clone the allaccess_api repository. Run the API, then run the allaccess_vue2 application and it should work.

## About
This project is created in Vue2 and connects with a Spring Boot REST API.
The goal of this project is to create a full-stack web application that allows employees to see their work schedule.
This project is a Work In Progress. Take a look at recent commits to follow the progress of this project.