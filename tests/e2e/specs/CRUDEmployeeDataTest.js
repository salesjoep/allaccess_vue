// https://docs.cypress.io/api/introduction/api.html

describe('User can recieve employee data from table', () => {
    it('Visits the app root url', () => {
        cy.visit('https://allaccess-vue.herokuapp.com/#/login')
        cy.contains('h1', 'All Access')
    })
    it('signs the user in', () => {
        cy.visit('https://allaccess-vue.herokuapp.com/#/login')
        cy.get('#username').type('hello')
        cy.get('#password').type('world')
        cy.wait(2000)
        cy.get('#login-button').click()
        cy.wait(2000)
        cy.reload()
        cy.get('.fa-users').click()
        cy.wait(2000)
        cy.contains('#mypass', 'sales');
        cy.wait(2000)
    })
})
