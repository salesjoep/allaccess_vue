// https://docs.cypress.io/api/introduction/api.html

describe('User can acess login screen', () => {
    it('Visits the app root url', () => {
        cy.visit('https://allaccess-vue.herokuapp.com/#/login')
        cy.contains('h1', 'All Access')
    })
    it('signs the user in & creates a new one', () => {
        cy.visit('https://allaccess-vue.herokuapp.com/#/login')
        cy.get('#username').type('hello')
        cy.get('#password').type('world')
        cy.wait(2000)
        cy.get('#login-button').click()
        cy.wait(2000)
        cy.reload()
        cy.get('.fa-users').click()
        cy.wait(2000)
        cy.get('#username').type('cypress')
        cy.get('#password').type('password')
        cy.get('#createUser').click()
        cy.wait(2000)
        cy.reload()
        cy.wait(2000)
        cy.contains('#mypass', 'password');
        cy.wait(2000)
    })
})
