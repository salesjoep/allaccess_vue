// https://docs.cypress.io/api/introduction/api.html

describe('Login Fail Test', () => {
    it('Visits the app root url', () => {
        cy.visit('https://allaccess-vue.herokuapp.com/#/login')
        cy.contains('h1', 'All Access')
    });
    it('does not log the user in', () => {
        cy.visit('https://allaccess-vue.herokuapp.com/#/login')
        cy.get('#username').type('false')
        cy.get('#password').type('credentials')
        cy.get('#login-button').click()
        cy.contains('.swal2-title', 'Error');
    })
})
