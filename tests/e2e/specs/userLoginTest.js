// https://docs.cypress.io/api/introduction/api.html

describe('Login System Tests', () => {
  it('Visits the app root url', () => {
    cy.visit('https://allaccess-vue.herokuapp.com/#/login')
    cy.contains('h1', 'All Access')
  });
  it('signs the user in', () => {
    cy.visit('https://allaccess-vue.herokuapp.com/#/login')
    cy.get('#username').type('hello')
    cy.get('#password').type('world')
    cy.get('#login-button').click()
    cy.contains('h2', 'Admin Panel - Home');
  })
})
